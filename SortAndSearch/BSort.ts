export class BSort {
    sort(input: number[]): number[] {
        let sorted = this.selection(input, 0);
        return sorted;
    }
    selection(input: number[], indexStart: number): number[] {
        for (let i = indexStart; i< input.length; i++) {
            let indexLeast = this.findLeast(input, i);
            let tmp = input[indexLeast];
            input[indexLeast] = input[i];
            input[i] = tmp;
        }
        return input;
    }
    findLeast(input: number[], indexStart: number): number {
        let compareValue = Infinity;
        let indexLeast = 0;
        for (let i = indexStart; i < input.length; i++){
            if (input[i] < compareValue) {
                indexLeast = i;
                compareValue = input[i]
            }
        }
        return indexLeast;
    }
}