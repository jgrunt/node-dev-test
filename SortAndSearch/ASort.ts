export class ASort {
    sort(input: number[]): number[] {
        let sorted = this.merge(input, 0, input.length - 1);
        return sorted;
    }
    merge(input: number[], left:number, right:number): number[] {
        if (left >= right) {
            return input
        }
        let mid = Math.floor((left + right) / 2);
        let mid2 = mid+1;
        let sortedLeft = [];
        let sortedRight = [];
        if (left < mid) {
            sortedLeft = this.merge(input, left, mid);
        } else {
            sortedLeft = [input[mid]]
        }
        if (right > mid2) {
            sortedRight = this.merge(input, mid2, right);
        } else {
            sortedRight = [input[mid2]]
        }
        let result = this.joinArrays(sortedLeft, sortedRight);
        return result;
    }
    joinArrays(arrayA: number[], arrayB: number[]): number[] {
        let indexA = 0;
        let indexB = 0;
        let merged = [];
        let counter = 0
        while (counter < arrayA.length + arrayB.length  ) {
            counter++;
            let valueA = arrayA[indexA] !== undefined ? arrayA[indexA]: Infinity;
            let valueB = arrayB[indexB] !== undefined ? arrayB[indexB]: Infinity;
            if (valueA > valueB) {
                merged.push(valueB);
                indexB++;
            } else {
                merged.push(valueA);
                indexA++;
            }
        }
        return merged;
    }
}