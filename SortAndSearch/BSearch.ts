export class BSearch {
    private static instance: BSearch;

    private operations: number = 0;
    
    private constructor() {
    }
    static getInstance() {
        if (!BSearch.instance) {
            BSearch.instance = new BSearch();
        }
        return BSearch.instance;
    }
    findIndexOf(input: number[], value: number) {
        return this.findIndex(input, value, -1, input.length)
    }
    private findIndex(input:number[], value:number, start: number, end: number): number {
        this.operations++;
        let compareIndex = Math.floor((start+end) / 2);
        let compareValue = input[compareIndex];
        if (compareIndex === start) {
            return -1
        } else if (value === compareValue){
            return compareIndex;
        } else if (compareValue > value){
            return this.findIndex(input, value, start, compareIndex);
        } else if (compareValue < value) {
            return this.findIndex(input, value, compareIndex , end);
        }
    }
    public getOperations(): number {
        return this.operations;
    }
}