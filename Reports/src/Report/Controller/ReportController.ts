import { Controller, Get, Param } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestSellers, IBestBuyers } from '../Model/IReports';

@Controller()
export class ReportController {
  constructor(private readonly orderMapper: OrderMapper) {}

  @Get('/report/products/:date')
  bestSellers(@Param('date') date: string): Promise<IBestSellers> {
    return this.orderMapper.bestSellersByDate(date);
  }

  @Get('/report/customer/:date')
  async bestBuyers(@Param('date') date: string): Promise<IBestBuyers> {
    return this.orderMapper.bestBuyersByDate(date);
  }
}
