export class Product {
  private _id: number;
  private _name: string;
  private _price: number;

  constructor({ id, name, price }) {
    this._id = id;
    this._name = name;
    this._price = price;
  }

  get id() {
    return this._id;
  }
  get name() {
    return this._name;
  }
  get price() {
    return this._price;
  }
  // todo implements model
}
