import { Customer } from './Customer';
import { Product } from './Product';

export class Order {
  private _number: string;
  private _customer: Customer;
  private _products: Product[];
  private _createdAt: string;

  constructor({ number, customer, products, createdAt }) {
    let date = new Date(createdAt).toISOString().substr(0, 10); // validation of date format
    if (date === createdAt) {
      createdAt = date;
    } else {
      throw new Error('Wrong createdAt parameter');
    }

    this._number = number;
    this._customer = customer;
    this._products = products;
    this._createdAt = createdAt;
  }

  get number() {
    return this._number;
  }
  get customer() {
    return this._customer;
  }
  get products() {
    return this._products;
  }
  get createdAt() {
    return this._createdAt;
  }
  get totalPrice() {
    let total = this._products.reduce(
      (previousValue, currentValue, index, array) => {
        return previousValue + currentValue.price;
      },
      0,
    );

    return total;
  }
  // todo implements model
}
