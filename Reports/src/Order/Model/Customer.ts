export class Customer {
  private _id: number;
  private _firstName: string;
  private _lastName: string;

  constructor({ id, firstName, lastName }) {
    this._id = id;
    this._firstName = firstName;
    this._lastName = lastName;
  }

  get id() {
    return this._id;
  }
  get firstName() {
    return this._firstName;
  }
  get lastName() {
    return this._lastName;
  }
  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }
  // todo implements model
}
