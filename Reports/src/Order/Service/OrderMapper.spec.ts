import { Test } from '@nestjs/testing';
import { OrderMapper } from './OrderMapper';
import { Order } from '../Model/Order';
import { ReportController } from '../../Report/Controller/ReportController';
import { Repository } from './Repository';

describe('OrderMapper', () => {
    let orderMapper: OrderMapper;
    let reportController: ReportController;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            controllers: [ReportController],
            providers: [OrderMapper, Repository],
        }).compile();

        orderMapper = module.get<OrderMapper>(OrderMapper);
        reportController = module.get<ReportController>(ReportController);
    });

    describe('getOrdersByDate', () => {
        it('should return an array of Orders', async () => {
            // jest.spyOn(orderMapper, 'getOrdersByDate').mockImplementation(() => result);

            expect(await orderMapper.getOrdersByDate('2019-08-01')).toHaveLength(0);
            expect(await orderMapper.getOrdersByDate('2019-08-07')).toHaveLength(2);
            // expect(await orderMapper.getOrdersByDate('2019-08-07')).toBeInstanceOf(Array<Order>);
        });
    });

    describe('getAllOrders', () => {
        it('should return all Orders', async () => {
            expect(await orderMapper.getAllOrders()).toHaveLength(5);
        });
    });

    describe('getProductById', () => {
        it('should return selected Product', async () => {
            expect(await orderMapper.getProductById(2)).toHaveProperty('price');
        });
        it('should throw error on missfind Product', async () => {
            expect(orderMapper.getProductById(-1)).rejects.toThrow();
        });
    });

    describe('bestSellersByDate', () => {
        it('should return "Black sport shoes"', async () => {
            expect(await orderMapper.bestSellersByDate('2019-08-07')).toHaveProperty('productName', "Black sport shoes");
        });
        it('should return "unknown"', async () => {
            expect(await orderMapper.bestSellersByDate('2019-08-079')).toHaveProperty('productName', "unknown");
        });
    });

    describe('bestBuyersByDate', () => {
        it('should return "John Doe"', async () => {
            expect(await orderMapper.bestBuyersByDate('2019-08-07')).toHaveProperty('customerName', "John Doe");
            expect(await orderMapper.bestBuyersByDate('2019-08-08')).toHaveProperty('customerName', "Jane Doe");
        });
        it('should return "unknown"', async () => {
            expect(await orderMapper.bestBuyersByDate('2019-08-079')).toHaveProperty('customerName', "unknown");
            expect(await orderMapper.bestBuyersByDate('')).toHaveProperty('customerName', "unknown");
        });
    });

});