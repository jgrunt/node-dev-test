import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Order } from '../Model/Order';
import { Product } from '../Model/Product';
import { Customer } from '../Model/Customer';
import { IBestSellers, IBestBuyers } from '../../Report/Model/IReports';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  private _customers: any[] = [];
  private _products: any[] = [];

  constructor() {}

  async getOrdersByDate(date: string): Promise<Order[]> {
    let orders = await this.getAllOrders();
    orders = orders.filter(o => o.createdAt === date);
    return Promise.resolve(orders);
  }

  async getAllOrders(): Promise<Order[]> {
    let orders = await this.repository.fetchOrders();
    let mappedOrders: Order[] = [];
    for (let order of orders) {
      let customer: Customer = await this.getCustomerById(order.customer);
      let products: Product[] = [];

      for (let product of order.products) {
        let tmp = await this.getProductById(product);
        products.push(tmp);
      }
      mappedOrders.push(
        new Order({
          number: order.number,
          customer,
          products,
          createdAt: order.createdAt,
        }),
      );
    }

    return Promise.resolve(mappedOrders);
  }

  async getProductById(id: number): Promise<Product> {
    let products = await this.repository.fetchProducts();
    for (let product of products) {
      if (product.id === id) {
        if (!this._products[id]) {
          this._products[id] = new Product(product);
        }
        return Promise.resolve(this._products[id]);
      }
    }
    return Promise.reject(new Error('unable to find product'));
  }

  async getCustomerById(id: number): Promise<Customer> {
    let customers = await this.repository.fetchCustomers();
    for (let customer of customers) {
      if (customer.id === id) {
        if (!this._customers[id]) {
          this._customers[id] = new Customer(customer);
        }
        return Promise.resolve(this._customers[id]);
        // return Promise.resolve(new Customer(customer));
      }
    }
    return Promise.reject(new Error('unable to find customer'));
  }

  async bestSellersByDate(date: string): Promise<IBestSellers> {
    let orders = await this.getOrdersByDate(date);
    let soldProducts = orders.map(o => o.products);
    let soldProductsArray = Array.prototype.concat(...soldProducts);
    let countingArr = [];

    soldProductsArray.forEach(product => {
      if (countingArr[product.id]) {
        countingArr[product.id].count++;
      } else {
        countingArr[product.id] = {
          count: 1,
          product: product,
        };
      }
    });

    countingArr.sort((a, b) => {
      return b.count - a.count;
    });

    // return Response.json(orders);
    if (countingArr[0]) {
      return {
        productName: countingArr[0].product.name,
        quantity: countingArr[0].count,
        totalPrice: countingArr[0].count * countingArr[0].product.price,
      };
    } else {
      return {
        productName: 'unknown',
        quantity: 0,
        totalPrice: 0,
      };
    }
  }

  async bestBuyersByDate(date: string): Promise<IBestBuyers> {
    let orders = await this.getOrdersByDate(date);
    let countingArr: { customer: Customer; totalPrice: number }[] = [];

    orders.forEach(order => {
      if (countingArr[order.customer.id]) {
        countingArr[order.customer.id].totalPrice += order.totalPrice;
      } else {
        countingArr[order.customer.id] = {
          customer: order.customer,
          totalPrice: order.totalPrice,
        };
      }
    });

    countingArr.sort((a, b) => {
      return b.totalPrice - a.totalPrice;
    });

    if (countingArr[0]) {
      return {
        customerName: countingArr[0].customer.fullName, // customer full name
        totalPrice: countingArr[0].totalPrice, // total amount spent on all products
      };
    } else {
      return {
        customerName: 'unknown',
        totalPrice: 0,
      };
    }
  }
}
